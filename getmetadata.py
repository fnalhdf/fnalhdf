#!/usr/bin/env python

import ROOT
import h5py
import numpy as np

#roottohdf5type
def geth5type(roottype):
  if roottype == "Float_t":
    return "float32"
  elif roottype == "UInt_t":
    return "int32"
  elif roottype == "Int_t":
    return "int32"
  elif roottype == "Bool_t":
    return "int8"
  elif roottype == "ULong_t":
    return "int64"
  else: 
    return "string"

def get_leaf_names_and_types(branchname, leaves):
  """Returns a dictionary mapping ROOT types to a list of leaf names that have that type.  
  """
  leaf_name_type={}
  for leaf in leaves:
     leaf_type = leaf.GetTypeName()
     if (not (leaf_type.startswith("bitset"))):
       leaf_name_type.setdefault(leaf.GetTypeName(), []).append(leaf.GetName())
  return leaf_name_type

def get_record_ranges_per_event_and_totalrecords(tree, leaves, num_entries_in_tree):
  """Return a tuple containing (np.array of tuples, number of records).
  The np.array's length is the number of events seen.  The entries in
  the np.array are the (begin, end) indices into the array of records
  corresponding to each event.
  """
  n_records = 0
  recordranges = []
  leaf = tree.GetLeaf(leaves[1].GetName())
  for ne in range(0, num_entries_in_tree):
    leaf.GetBranch().GetEntry(ne)
    num_elements_in_leaf = leaf.GetNdata()
    recordranges.append((n_records, n_records+num_elements_in_leaf))
    n_records = n_records + num_elements_in_leaf
  recordranges = np.array(recordranges)
  return recordranges, n_records


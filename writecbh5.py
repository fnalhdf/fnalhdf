#!/usr/bin/env python

import ROOT
import h5py
import numpy as np
import getmetadata as md
import os, sys

MAX_RECORDS = 100000
#Layout specific functions
def create_inmemory_dataset(leaf, num_entries_in_tree, key, n_records):
  """Return np.array containing records.
  The type carried in the array is determined by 'key'.
  The length of the np.array is determined by n_records.
  """
  records = np.empty([n_records], md.geth5type(key))
  prev = 0
  for ne in range(0,num_entries_in_tree):
    leaf.GetBranch().GetEntry(ne)
    num_elements_in_leaf = leaf.GetNdata()
    for n in range(0, num_elements_in_leaf):
      records[n+prev] = leaf.GetValue(n)
    prev = prev + num_elements_in_leaf
  return records

def create_h5_dataset(h5group, tree, branch, num_entries_in_tree, eventnum, runnum, lumisec):
  """Returns None.  Takes an already-open h5group, and writes to it the
     datasets on the given branch in the given tree.

  Branch must be a branch on the given tree. The branch must contain
  ONLY leaves, not other branches.
  """
  leaves = branch.GetListOfBranches()
  record_ranges_per_event, n_records = md.get_record_ranges_per_event_and_totalrecords(tree, leaves, num_entries_in_tree)
  types_and_names = md.get_leaf_names_and_types(branch.GetName(), leaves)
#  print branch.GetName(), n_records
#  print types_and_names
  for python_type, leaf_names in types_and_names.iteritems():
    for leaf_name in leaf_names:
      leaf = tree.GetLeaf(leaf_name)
      #if (python_type != "Bool_t"):
      dset = create_inmemory_dataset(leaf, num_entries_in_tree, python_type, n_records)
      #print leaf_name + " dataset created, with type: " + python_type
        #dtype=md.geth5type(python_type)
      h5group.create_dataset(leaf_name, data=dset, compression='gzip', chunks=True)
  d = create_uniqueid(eventnum, record_ranges_per_event)
  h5group.create_dataset(branch.GetName()+".evtNum", data=d, compression='gzip', chunks=True)
  d = create_uniqueid(runnum, record_ranges_per_event)
  h5group.create_dataset(branch.GetName()+".runNum", data=d, compression='gzip', chunks=True)
  d = create_uniqueid(lumisec, record_ranges_per_event)
  h5group.create_dataset(branch.GetName()+".lumisec", data=d, compression='gzip', chunks=True)

def create_uniqueid(unique_id, record_ranges_per_event): 
  evtNum = [] #np.empty([len(eventnum)], md.geth5type("string"))  
  for i in range(0, len(unique_id)):
    st = record_ranges_per_event[i][0]
    end = record_ranges_per_event[i][1]
    if (st != end):  
      for j in range(st, end):
        #unique_id = "%d, %d, %d" %(runnum[i],lumisec[i], eventnum[i]) 
        evtNum.append(unique_id[i])
  return evtNum


def write_column_based_h5file(tree, output_filename, debug=False):
  """Returns None.

  Create an HDF5 file named 'output.h5', and write into it all the
  data read from the given tree. Creates one HDF5 group per branch in
  the tree and one HDF5 dataset in the created HDF5 group per leaf in
  that branch.
  """
  branches = tree.GetListOfBranches()
  num_entries_in_tree = tree.GetEntries()
  branches_used = ['Info', 'GenEvtInfo', 'Muon', 'Electron', 'Tau', 'Photon', 'AK4Puppi', 'CA15Puppi', 'AddCA15Puppi']
  eventnum = create_inmemory_dataset(tree.GetLeaf("evtNum"), num_entries_in_tree, "UInt_t", num_entries_in_tree)
  runnum = create_inmemory_dataset(tree.GetLeaf("runNum"), num_entries_in_tree, "UInt_t", num_entries_in_tree)
  lumisec = create_inmemory_dataset(tree.GetLeaf("lumiSec"), num_entries_in_tree, "UInt_t", num_entries_in_tree)
  #print eventnum 

  output_file = h5py.File(output_filename, 'w')
  if debug:
      print "Opened HDF file: %s" % output_file
  for branch in branches:
    if (branch.GetName() in branches_used):  
      create_h5_dataset(output_file.create_group(branch.GetName()),
                      tree,
                      branch,
                      num_entries_in_tree, 
                      eventnum, runnum, lumisec)
  output_file.close()

def do_work(input_filename, output_filename, tree_name):
  print input_filename
  f = ROOT.TFile.Open(input_filename)
  tree = f.Get(tree_name)
  if (not tree):
    print "invalid tree"
  #Create one file, one group per branch and one dataset per leaf in that branch
  else :
    debugging = True
    if (os.path.isfile(output_filename)):
      print "Already converted: %s" % output_filename
    else:
      write_column_based_h5file(tree, output_filename, debugging)

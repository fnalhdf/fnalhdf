#!/usr/bin/env python

import ROOT
import h5py
import numpy as np
import getmetadata as md

def createMB(tree, leaves, branches, num_entries_in_tree):
   file = h5py.File("hdf5out/WW_13TeV_pythia8_1_MB.h5", 'w')
   #file = h5py.File("hdf5out/bacon_MB.h5", 'w')
   for branch in branches:
      h5group = file.create_group(branch.GetName())
      leavesinbranch = branch.GetListOfBranches()
      x, n_records = md.get_recordranges_and_totalrecords(tree, leavesinbranch, num_entries_in_tree)
      names = md.get_leaf_names_and_types(branch.GetName(), leavesinbranch)
      dataset = h5group.create_dataset("eventid", (num_entries_in_tree, 2), dtype ="int", data=x)
      for key, values in names.iteritems():
        y = np.empty([n_records, len(values)], md.geth5type(key))
        i = -1
        for value in values:
          i = i+1
          prev = 0
          leaf = tree.GetLeaf(value)
          for ne in range(0, num_entries_in_tree):
            leaf.GetBranch().GetEntry(ne)
            num_elements_in_leaf = leaf.GetNdata()
            for n in range (0, num_elements_in_leaf):
              y[prev][i] = leaf.GetValue(n)
              prev = prev + 1
        dataset = h5group.create_dataset(branch.GetName()+"_"+key, dtype=md.getH5type(key), data=y)
        file[branch.GetName()][branch.GetName()+"_"+key].attrs['column_names'] = values
   file.close()


def do_work():
  f = ROOT.TFile.Open("Output.root")
  tree = f.Get("Events")

  #matrix layout for same type within each group
  createMB(tree, leaves, branches, num_entries_in_tree)


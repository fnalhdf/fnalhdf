import writecbh5 as r2h5
import os, sys

if __name__ == '__main__':
    #read the file names from an input file
    #infilename = 
    #outfilename = 
    indir = "/scratch/ssehrish/"
    outdir = "/scratch/ssehrish/h5out/"
    dirname= sys.argv[1]
    if (os.path.exists(outdir+dirname)):
      print "Directory already exists"
    else:
      os.mkdir(outdir+dirname, 0755)
      print "Directory created"
    #f = open('/home/ssehrish/fnalhdf/indir/set1')
    #for dirname in iter(f): 
    for fname in os.listdir(indir+dirname):
        #print indir+dirname+"/"+fname
        #print outdir+dirname+fname.strip('.root')+".h5"
        r2h5.do_work(indir+dirname+"/"+fname, outdir+dirname+"/"+fname.strip('.root')+".h5", "Events")
        print "Finished writing " + outdir+dirname+fname.strip('.root')+".h5"
#    r2h5.do_work("/scratch-shared/ssehrish/CMS-root/WW_13TeV_pythia8/WW_13TeV_pythia8_0.root", "WW_13TeV_pythia8_0.h5", "Events")
